# Spektrogramm für Multischichtsysteme
Das jupyter notebook in diesem Repository enthält Simulationen, die ich für meine Masterarbeit erstellt habe. Sie verwenden das exzellente Pythonmodul [tmm](https://pypi.org/project/tmm/). Die daten wurden experimentell bestimmt. Details zum theoretischen Hintergrund und zu den Messmethoden können in meiner Masterarbeit nachgelesen werden [1].

Um das notebook online lesen zu können, kann auf den Service von [nbviewer](https://nbviewer.org/urls/codeberg.org/LibrEars/Spektrogramm_Multischichtsystem/raw/branch/master/Spektrogramm_Multischichtsystem.ipynb) zurückgegriffen werden.

# Referenzen
[1] Seiberlich, M. Ultradünne dielektrische Elastomeraktuatoren für optische Anwendungen. Universitätsbibliothek Heidelberg (2017).

# Lizenz
Die Inhalte dieses Repositories sind unter der Creative Commons Lizenz mit Namensnennung 4.0 International (CC-BY 4.0) lizenziert https://creativecommons.org/licenses/by/4.0/deed.de
